package juego;
//comentario
import java.awt.Image;
import java.util.Random;

//if princesa < moneda+1 && princesa > moneda-1; 

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;

	// Variables y mtodos propios de cada grupo
	// ...
	// double tick;
	// double diametro;
	Random rand =new Random();
	Vidas vidas;
	Puntaje puntaje;
	Fondo fondo;
	Montana[] montana;
	Princesa princesa;
	Nube[] nube;
	Enemigos[] enemigos;
	Piso[] piso;
	Obstaculos[] obstaculos;
	Bola bola;
	Arbustos[] arbustos;
	Moneda[] moneda;
	Image imagen;

	// Prueba contador de ticks(no se si es necesario//
	int nivel = 1;
	int tiempo = 0;
	int tick = 0;
	int savetick=0;
	boolean contadorTicks;

	Juego() {
		// Inicializa el objeto entorno

		this.bola = null;

		this.entorno = new Entorno(this, "Super Elizabeth Sis - Grupo - v6", 800, 600);

		// Inicializar lo que haga falta para el juego
		// ...

		this.vidas = new Vidas(20, 20, 50, 60);
		this.puntaje = new Puntaje(700, 20, 50, 60);
		this.princesa = new Princesa(150, 384, 90, 70);
		this.bola = new Bola(princesa.getX(), princesa.getY(), 20);
		// this.piso = new Piso(400, 500, 350, 800);// (400,600,350,800
		this.fondo = new Fondo(400, 300, 600, 800);

		this.piso = new Piso[100];
		for (int i = 0; i < this.piso.length; i++) {
			this.piso[i] = new Piso(500 + (600 * i), 500, 90, 70);
		}

		this.moneda = new Moneda[100];
		for (int i = 0; i < this.moneda.length; i++) {
			this.moneda[i] = new Moneda(500 + (700 * i)+rand.nextInt(70), 50+rand.nextInt(100), 90, 70);
		}

		this.arbustos = new Arbustos[100];
		for (int i = 0; i < this.arbustos.length; i++) {
			this.arbustos[i] = new Arbustos(200 + (400 * i), 360, 90, 70);
		}
		this.vidas.setvida(3);

		this.obstaculos = new Obstaculos[3];
		for (int i = 0; i < this.obstaculos.length; i++) {
			this.obstaculos[i] = new Obstaculos(700 + (700 * i)+rand.nextInt(nivel*10), 380, 90, 70);
		}
		this.enemigos = new Enemigos[100];// (700,400,90,70);
		for (int i = 0; i < this.enemigos.length; i++) {
			this.enemigos[i] = new Enemigos(700 + (400 * i)+rand.nextInt(300), 400, 90, 70);
		}

		this.nube = new Nube[2];
		for (int i = 0; i < this.nube.length; i++) {
			this.nube[i] = new Nube(300 + (650 * i), 50, 50, 70);
		}

		this.montana = new Montana[3];
		for (int i = 0; i < this.montana.length; i++) {
			this.montana[i] = new Montana(300 + (800 * i), 250, 50, 70, princesa.getX());

		}

		// Inicia el juego!
		this.entorno.iniciar();
		princesa.getLetsGo().start();
	}

	// metodo entre princesa y enemigo//
	boolean colisionPrincesaEnemigo(Enemigos enemigo) {
		if (this.princesa.getY() + this.princesa.getAlto() / 2 > enemigo.getY() - enemigo.getAlto() / 2
				&& this.princesa.getX() + this.princesa.getAncho() / 2 > enemigo.getX() - enemigo.getAncho() / 2
				&& this.princesa.getY() - this.princesa.getY() / 2 < enemigo.getY() + enemigo.getAlto() / 2
				&& this.princesa.getX() - this.princesa.getAncho() / 2 < enemigo.getX() + enemigo.getAncho() / 2)
			return true;
		else
			return false;
	}

	// metodo colision entre princesa y obstaculo//
	boolean colisionPrincesaObst(Obstaculos obs) {
		if (this.princesa.getY() + this.princesa.getAlto() / 2 > obs.getY() - obs.getAlto() / 2
				&& this.princesa.getX() + this.princesa.getAncho() / 2 > obs.getX() - obs.getAncho() / 2
				&& this.princesa.getY() - this.princesa.getY() / 2 < obs.getY() + obs.getAlto() / 2
				&& this.princesa.getX() - this.princesa.getAncho() / 2 < obs.getX() + obs.getAncho() / 2)

			return true;
		else

			return false;
	}
    //metodo colision entre Princesa y moneda
	boolean colisionPrincesaMoneda(Moneda mon) {
		if (this.princesa.getY() + this.princesa.getAlto() / 2 > mon.getY() - mon.getAlto() / 2
				&& this.princesa.getX() + this.princesa.getAncho() / 2 > mon.getX() - mon.getAncho() / 2
				&& this.princesa.getY() - this.princesa.getY() / 2 < mon.getY() + mon.getAlto() / 2
				&& this.princesa.getX() - this.princesa.getAncho() / 2 < mon.getX() + mon.getAncho() / 2)

			return true;
		else

			return false;
	}
//setvelocidad//}
void setnivel(int tiempo) {
if (tiempo%1000==0&& tiempo<4000) {
	//tiempo=0;
	 nivel += 1;
	 System.out.println("nivel" + nivel);
}
	
}
	/**
	 * Durante el juego, el mtodo tick() ser ejecutado en cada instante y por lo
	 * tanto es el mtodo ms importante de esta clase. Aqu se debe actualizar el
	 * estado interno del juego para simular el paso del tiempo (ver el enunciado
	 * del TP para mayor detalle).
	 */

	// *****************TICKS INSTANTE DE TIEMPO***********************//

	public void tick() {
		
		if(puntaje.getValor()>=7)
		{     
			this.imagen = Herramientas.cargarImagen("youWin.png");
			//return;
		}
		else if(vidas.getLife()<0) 
		{
			//vidas.getGameOver().start();
			this.imagen = Herramientas.cargarImagen("TheEnd.png");
			return;
		}
		
		
// CONTADOR DE TICKS//
		tiempo=tiempo+1;
		
		if (contadorTicks == true) {
			tick = tick + 1;
		}
		
	

//VELOCIDAD//
		setnivel(tiempo);
		
		
// FONDO 

		// DIBUJA FONDO//
		this.fondo.dibujar(this.entorno);

//MONTANAS// DIBUJA Y MUEVE MONTAAS//

		for (int i = 0; i < this.montana.length; i++) {
			this.montana[i].mover();
			this.montana[i].dibujar(this.entorno);
			if (this.montana[i].getx() <= -300) {
				this.montana[i].setX(1500);
			}
			// podemos crear un bucle
		}

//PISO		// DIBUJA PISO //

		// this.piso.dibujar(this.entorno);
		for (int i = 0; i < this.piso.length; i++) {
			this.piso[i].mover();
			this.piso[i].setV(nivel);
			this.piso[i].dibujar(this.entorno);
			if (this.piso[i].getx() <= -50) {
				this.piso[i].setX(1050);
			}
		}

		// Procesamiento de un instante de tiempo
		// ...

//PRINCESA		// MOVIENTO SOBRE EJE X DE PRINCESA//
		if (this.entorno.estaPresionada(this.entorno.TECLA_IZQUIERDA) && princesa.getX() > 20) {

			this.princesa.moverizquierda();
		}

		if (this.entorno.estaPresionada(this.entorno.TECLA_DERECHA) && princesa.getX() < this.entorno.ancho() - 450) {
			this.princesa.moverderecha();
		}

// SALTO PRINCESA//
		if (this.entorno.estaPresionada(this.entorno.TECLA_ARRIBA) && princesa.isCaida() == false) {
			princesa.setSalto(true);

		}

		if (princesa.isCaida() == true) {
			this.princesa.moverabajo();
			if (princesa.getY() == 380) {
				this.princesa.moverderecha();
				princesa.setCaida(false);
			}

		}

		if (princesa.isSalto() == true) {
			this.princesa.moverarriba();
			if (princesa.getY() == 376) {
				princesa.getJump().start();
			}
			if (princesa.getY() == 124) {
				princesa.setSalto(false);
				princesa.setCaida(true);

			}

		}

		// DIBUJA PRINCESA//
		this.princesa.dibujar(this.entorno);

//OBSTACULO // DIBUJA Y MUEVE OBSTACULOS// falta ciclo infinito
		for (int i = 0; i < this.obstaculos.length; i++) {
			this.obstaculos[i].mover();
			this.obstaculos[i].setV(nivel);
			this.obstaculos[i].dibujar(this.entorno);
			if (this.obstaculos[i].getX() <= -50) {
				this.obstaculos[i].setX(1500);// para cumplir requerimiento obligatorio .4

			}
			// COLISION PRINCESA OBSTACULO//

			if (this.colisionPrincesaObst(obstaculos[i]) && tick == 0) {

				System.out.println("colision princesa obstaculo");

				this.vidas.restarvida(1);
				this.princesa.perdervida();
				contadorTicks = true;
				System.out.println(tick);

			}

			if (tick > 250) {
				contadorTicks = false;
				tick = 0;
				System.out.println(tick);
			}
		}

//ENEMIGOS		// DIBUJA Y MUEVE ENEMIGOS//

		for (int i = 0; i < this.enemigos.length; i++) {
			this.enemigos[i].setV(-nivel);
			this.enemigos[i].mover();
			this.enemigos[i].dibujar(this.entorno);

// COLISION PRINCESA-ENEMIGO//
			if (colisionPrincesaEnemigo(enemigos[i]) && vidas.getLife()>0) {
				System.out.println("colision princesa-enemigo");
				this.enemigos[i].setX(-30);
				this.enemigos[i].mover();
				this.princesa.perdervida();
				princesa.getDeath().start();
				this.vidas.restarvida(1);
				this.princesa.perdervida();
			}

// COLISION BOLA-ENEMIGO//

			if (this.enemigos[i].getX() <= this.bola.getX() && this.princesa.getX() < this.enemigos[i].getX()) {
				if (this.bola.getX() == this.princesa.getX()) {
					this.puntaje.sumarpuntaje(0);
				} else

				{
					System.out.println("bola-enemigo");
					this.bola = new Bola(princesa.getX(), princesa.getY(), 20);
					this.enemigos[i].setX(-30);
					this.puntaje.sumarpuntaje(5);
				}

			}

		}

//DISPARO		// DIBUJA Y MUEVE BOLA//
		if (this.entorno.estaPresionada(this.entorno.TECLA_ESPACIO)&&this.princesa.isSalto()==false&&princesa.isCaida()==false)
			
		{
			//princesa.getDisparo().start(); // DEBERIA ACORTAR EL TONO PARA QUE SEA UN SOLO DISPARO
			this.bola.dibujar(this.entorno);
			this.bola.setFuego(true);// para que se mantenga dibujada despues de tocar la tecla
			this.bola.mover();
		}
		// llega al limite y se crea una nueva bola
		if (this.bola.getX() > 801) {
			this.bola = new Bola(princesa.getX(), princesa.getY(), 20);
		//sonido disparo//
			
			if (this.bola.getX() > this.princesa.getX()+1) {
				princesa.getDisparo().start();
			}
				//this.bola = new Bola(princesa.getX(), princesa.getY(), 20);
		}
		// disparo se mueve
		if (this.bola.isFuego()) {
		//	princesa.getDisparo().start();
			this.bola.dibujar(this.entorno);
			this.bola.mover();

		}
// COLISION OBJETO-ENEMIGO//
		// for (int j = 0; j < this.obstaculos.length; j++) {
//
		// for (int i = 0; i < this.enemigos.length; i++) {
		// if (this.enemigos[i].getX() - 5 == (int) this.obstaculos[j].getx()) {
		// System.out.println("colision obstaculo- enemigo");
		// this.enemigos[i].moverderecha();
		// }
		// }
		// }
//ARBUSTOS//
		for (int i = 0; i < this.arbustos.length; i++) {
			this.arbustos[i].mover();
			this.arbustos[i].setV(nivel);
			this.arbustos[i].dibujar(this.entorno);
		}
//MONEDA//
		for (int i = 0; i < this.moneda.length; i++) {
			this.moneda[i].mover();
			this.moneda[i].dibujar(this.entorno);
			if (colisionPrincesaMoneda(moneda[i])) {
				puntaje.sumarpuntaje(1);
				this.moneda[i].setX(-900);
			}
		}
//NUBES		// DIBUJA Y MUEVE NUBES//
		for (int i = 0; i < this.nube.length; i++) {
			this.nube[i].mover();
			this.nube[i].dibujar(this.entorno);
			if(vidas.getLife()>0)
			{
				if (colisionPrincesaMoneda(moneda[i])) {
					puntaje.sumarpuntaje(1);
					this.moneda[i].setX(-900);
				}
			} else
			{
				if (colisionPrincesaMoneda(moneda[i])) 
				{
					puntaje.sumarpuntaje(0);
					this.moneda[i].setX(-900);
				}
			}
		}

//VIDAS		// *********DIBUJA  VIDAS Y PUNTAJE*******************//
		this.vidas.dibujar(this.entorno);
		this.puntaje.dibujar(this.entorno);

	}

//	@SuppressWarnings("unused")
//	public static void main(String[] args) {
//		Juego juego = new Juego();
//	}
}

