package juego;

import java.awt.Color;
import java.awt.Image;

import javax.sound.sampled.Clip;

import entorno.Entorno;
import entorno.Herramientas;

public final class Vidas {
	private int x;
	private int y;
	private int alto;
	private int ancho;
	private Image imagen;
	private int life;
	private Clip gameOver;

	public Vidas(int x, int y, int alto, int ancho) {
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.imagen = Herramientas.cargarImagen("corazon.png");
	//	this.gameOver = Herramientas.cargarSonido("GO.waw");
	}

	// SETTER
	public void setvida(int num) {
		this.life = num;

	}

	// public void restarvida(int num) {
	// this.life = this.life - num;

	// }
	public void restarvida(int num) {
		if (life > 0) {
			this.life = this.life - num;
		} else {
			this.imagen = Herramientas.cargarImagen("TheEnd.png");
			//getGameOver().start();
		}
	}

	// GETTER
	int getLife()
	{
		return life;
	}
	public String getlife() {
		String vidaTotal = Integer.toString(life);
		return vidaTotal;
	}

	public Clip getGameOver() {
		return gameOver;
	}

	// DIBUJAR
	public void dibujar(Entorno e) {

		e.dibujarImagen(imagen, this.x, this.y, 0);
		e.cambiarFont("Arial", 25, Color.white);
		e.escribirTexto(getlife(), this.x + 30, this.y + 7);
	}
}
