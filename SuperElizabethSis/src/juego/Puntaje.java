package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public final class Puntaje {
	private int x;
	private int y;
	private int alto;
	private int ancho;
	private Image imagen;
	private int valor;

	public Puntaje(int x, int y, int alto, int ancho) {
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.imagen = Herramientas.cargarImagen("point.png");
	}

	// Setter

	public void setpuntaje(int num) {
		this.valor = num;

	}

	public void sumarpuntaje(int num) {
		this.valor = this.valor + num;
	}

	// Getter//

	int getValor()
	{
		return valor;
	}
	public String getpoints() {
		String puntajeTotal = Integer.toString(valor);
		return puntajeTotal;
	}

	// dibujar//

	public void dibujar(Entorno e) {

		e.dibujarImagen(imagen, this.x, this.y, 0);
		e.cambiarFont("Arial", 25, Color.white);
		e.escribirTexto(getpoints(), this.x + 30, this.y + 7);
	}
}
