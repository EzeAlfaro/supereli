package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Arbustos {
	private double x;
	private double y;
	private double alto;
	private double ancho;
	private Image imagen;
	private int velocidad;

	public Arbustos(double x, double y, double alto, double ancho) {
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.imagen = Herramientas.cargarImagen("arbusto (2).png");
		this.velocidad= 0;
	}

	public void dibujar(Entorno e) {

		e.dibujarImagen(imagen, this.x, this.y, 0);

	}

	public void mover() {
		this.x -= 1 + velocidad;

	}

public void setV(int i) {
	this.velocidad= i;
	// TODO Auto-generated method stub

}

}
