package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Bola {
	private double x;
	private double y;
	private double diametro;
	private int velocidad;
	private Image imagen;

	Bola(double e, double d, double diametro) {
		this.x = e;
		this.y = d;
		this.diametro = diametro;
		this.velocidad = +9;
		this.imagen = Herramientas.cargarImagen("fuego100.gif");
	}

	public void mover() {
		this.x = this.x + this.velocidad;
	}

	public void dibujar(Entorno e) {
	e.dibujarImagen(imagen, x, y, 0);	//e.dibujarCirculo(this.x, this.y, this.diametro, Color.RED);
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getDiametro() {
		return diametro;
	}

	public boolean isFuego() {
		return fuego;
	}

	public void setFuego(boolean fuego) {
		this.fuego = fuego;
	}

	private boolean fuego = false;
	//public boolean explosion = false;

}
